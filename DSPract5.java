
//package dspract5;

import java.rmi.*; 
import java.rmi.server.*; 
import java.sql.*;

public class DSPract5 extends UnicastRemoteObject implements DSPract5interface 
{  
        Connection con;
        public Statement stmt;
        ResultSet rs;
        DSPract5() throws RemoteException 
        { 
            super(); 
            try
            { 
                Class.forName("oracle.jdbc.driver.OracleDriver");
                con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1523:ORCL","system","system");
                
                stmt= con.createStatement();
                String sql = "CREATE TABLE student2 " +
                           "(id INTEGER not NULL, " +
                           "name VARCHAR(255), " + 
                           "des VARCHAR(255)," +
                           " PRIMARY KEY ( id ))"; 
                 stmt.executeUpdate(sql);
                 rs=stmt.executeQuery("select * from student2");
            }
            catch(Exception e)
            {
                System.out.println(""+e);
            }
        } 
        public String insert(int empno, String ename, String job) throws Exception 
        {
            String sql=sql = "INSERT INTO student2 " +
                       "VALUES ("+empno+",'"+ename+"','"+job+"')";
            int y=stmt.executeUpdate(sql);
            return "DATA INSERTED :)";
        }
        
        
        
        @Override
        public String UpdateName(int id,String name) throws Exception 
        {
                
                rs=stmt.executeQuery("select * from student2");
            
               String sql = "update student2 set name='"+name+"' where id="+id;
                    stmt.executeUpdate(sql);
                    return "NAME UPDATED :)"; 
            
            
        }
        @Override
        public String UpdateDes(int id,String desi) throws Exception 
        {
            
                rs=stmt.executeQuery("select * from student2");
            
                    String sql = "update student2 set des='"+desi+"' where id="+id;
                    stmt.executeUpdate(sql);
                    return "DESIGNATION UPDATED :)";
               
        }
        public String delete(int id)throws Exception
        {
             rs=stmt.executeQuery("select * from student2");
            while(rs.next())
            {                   
                if(rs.getInt(1)==id)
                {
                    String del= "DELETE FROM student2 WHERE id="+id;
                    int z=stmt.executeUpdate(del);
                    return "RECORD DELETED :)";
                }
                else
                {
                    return "ID IS NOT PRESENT :(";
                }
            }  
            return "";
            
        }
        public String selectone(int id)throws Exception
        {
            String hh="";
            rs=stmt.executeQuery("select * from student2 where id="+id);
            while(rs.next())
            {   
                if(rs.getInt(1)==id)
                {
                   hh=hh+rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3)+"\n"; 
                   
                }
                else
                {
                    hh="ID IS NOT PRESENT :(";
                }
            }
            return hh;
        }
        public String selectall()throws Exception
        {       
            String hh="";
            rs=stmt.executeQuery("select * from student2");
            int rowcount=0;
            while(rs.next())
            {
                   hh=hh+rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3)+"\n"; 
                   rowcount++;
            }
            if(rowcount==0){
                return "NO DATA PRESENT :(";
            }
            return hh;
        }    

    @Override
    public String checkid(int id) throws Exception {
        rs=stmt.executeQuery("select * from student2");
            while(rs.next())
            {
                   
                if(rs.getInt(1)==id)
                {
                    return "1";
                }
                else
                {
                    //
                }
            } 
            return "0";
    }

    
}   

